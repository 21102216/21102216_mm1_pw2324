import React from "react";
import "./style.css";

export const Box = () => {
    return (
        <div className="box">
        <div className="pemesanan">
            <div className="overlap">
            <div className="text-wrapper">Form Pemesanan Tiket</div>
            <div className="overlap-group">
                <div className="form-label-full-name">Pesan Tiket</div>
                <div className="form-input">
                <div className="div-placeholder">
                    <div className="div">
                    <div className="text-wrapper-2">2</div>
                    <img className="vector" alt="Vector" src="vector.svg" />
                    </div>
                </div>
                </div>
            </div>
            <div className="overlap-2">
                <div className="form-label-full-name-2">Nama Pemesan</div>
                <div className="div-placeholder-wrapper">
                <div className="div-wrapper">
                    <div className="div">
                    <div className="text-wrapper-2">Chesa</div>
                    </div>
                </div>
                </div>
            </div>
            <div className="overlap-3">
                <div className="form-label-full-name-3">Tanggal</div>
                <div className="input">
                <div className="div-date-time-edit">
                    <div className="overlap-group-2">
                    <div className="spinbutton-month-mm">mm</div>
                    <div className="text-wrapper-3">/</div>
                    <div className="spinbutton-day-dd">dd</div>
                    <div className="text-wrapper-4">/</div>
                    <div className="spinbutton-year-yyyy">yyyy</div>
                    </div>
                </div>
                <img className="div-picker" alt="Div picker" src="div-picker.svg" />
                </div>
            </div>
            <div className="overlap-4">
                <div className="form-label-full-name-4">Pemandu</div>
                <div className="form-input-2">
                <div className="div-placeholder-2">
                    <div className="text-wrapper-2">Nurhayati</div>
                    <img className="vector" alt="Vector" src="vector-2.svg" />
                </div>
                </div>
            </div>
            <div className="form-label-nationaly">Pilihan Paket</div>
            <div className="form-input-3">
                <div className="div-placeholder-3">
                <div className="text-wrapper-5">Paket Wisata 1</div>
                <img className="vector" alt="Vector" src="image.svg" />
                </div>
            </div>
            <div className="overlap-5">
                <div className="form-input-4">
                <div className="div-placeholder-2">
                    <div className="text-wrapper-2">QRIS</div>
                    <img className="vector" alt="Vector" src="vector-3.svg" />
                </div>
                </div>
                <div className="form-label-phone">Metode Pembayaran</div>
            </div>
            <div className="overlap-6">
                <div className="form-input-5">
                <div className="div-placeholder-2">
                    <div className="text-wrapper-6">chesaolivia99@gmail.com</div>
                </div>
                </div>
                <div className="form-label-phone-2">Email</div>
            </div>
            <div className="overlap-7">
                <div className="form-input-2">
                <div className="div-placeholder-2">
                    <div className="text-wrapper-7">085932441121</div>
                </div>
                </div>
                <div className="form-label-phone">No Telepon</div>
            </div>
            <button className="button">
                <div className="text-wrapper-8">Pesan Sekarang</div>
            </button>
            </div>
        </div>
        </div>
    );
    };
